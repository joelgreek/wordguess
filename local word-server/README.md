1. `cd` into the `random-word-server` directory (where this README is located).
2. Run `npm install` 
3. Run `npm start`. The server will be found at [http://localhost:3030]
